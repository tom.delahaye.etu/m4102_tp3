### API et représentation des données

Nous pouvons tout d'abord réfléchir à l'API REST que nous allons offrir pour la ressource *commande*. Celle-ci devrait répondre aux URI suivantes :

| URI            	   | Opération   | MIME 										 | Requête 		     | Réponse                                                            |
| :--------------	   | :---------- | :---------------------------------------------| :--               | :----------------------------------------------------------------- | 
| /commande      	   | GET         | <-application/json<br><-application/xml       |                   | liste des commandes (C2) et sa liste de pizzas                     |
| /commande/{id} 	   | GET         | <-application/json<br><-application/xml       |                   | une commande ou 404                                                |
| /commande/{id}/nom   | GET         | <-text/plain                                  |                   | le nom de l'acheteur ou 404                                        | 
| /commande            | POST        | <-/->application/json                         | Commande(C2)      | Nouvelle commande(C2)<br>409 si la commande existe déjà (même nom) |
| /commande/{id}       | DELETE      |                                               |                   |                                                                    |


Une commande comporte uniquement une liste des pizzas, un identifiant, un nom et un prénom et la liste de pizzas. Sa
représentation JSON (C2) prendra donc la forme suivante :

    {"id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "prénom":"Tom",
      "nom": "Delahaye",
      "list": ["Thon","Orientale"]}  
    }  


Lors de la création, l'identifiant n'est pas connu car il sera fourni
par le JavaBean qui représente une commande. Aussi on aura une
représentation JSON (C1) qui comporte uniquement le nom, prénom et la liste des pizzas :

    { "prénom":"Tom",
      "nom": "Delahaye",
      "list": ["Thon","Orientale"]}  
    }